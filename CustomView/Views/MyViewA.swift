//
//  MyViewA.swift
//  自定义 上图下文 View(纯代码)
//
//  Created by Sven on 2020/8/12.
//  Copyright © 2020 Sven. All rights reserved.
//

import UIKit

class MyViewA: UIView {
    var imageView = UIImageView()
    var label     = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        addSubview(imageView)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .green
        addSubview(label)
    }
    
    // MARK: - layout
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let imageViewW: CGFloat = 60
        let imageViewX = (bounds.width-60)/2
        imageView.frame = CGRect(x: imageViewX, y: 8,width: imageViewW, height: imageViewW)
        let labelY = imageViewW + 8 + 8
        label.frame = CGRect(x: 0, y: labelY, width: bounds.width, height: 17)
    }
    
}

