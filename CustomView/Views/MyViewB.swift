//
//  MyViewB.swift
//  xib + 代码 自定义 View
//
//  Created by Sven on 2020/8/12.
//  Copyright © 2020 Sven. All rights reserved.
//
/// 1. 生成同名 swift 文件和 xib 文件
/// 2. 将 xib 的 FileOwner 的 class 设置为自定义 view class
/// 3. 在代码文件内 通过 UINib 加载并实例化 view ,添加到自定义 view上（所以自定义 View 相当于有两层 View）
/// 4. 在 layouSubviews方法内调整 contentView 的 frame,让其与自身 bounds 一致。

import UIKit

class MyViewB: UIView {
    private var contentView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // [重点] 这里需要设置 contenView 的 frame，否则 contenView 就和 xib 大小一致，在实际使用中大小与预期不一致
        contentView.frame = bounds
    }
    
    fileprivate func commonInit() {
        loadnib()
    }
    
    fileprivate func loadnib() {
        let nib = UINib(nibName: String(describing: MyViewB.self), bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return
        }
        addSubview(view)
        // 【重要】将背景色置为 clear, 这样不会影响父控件设置背景色。
        view.backgroundColor = .clear
        contentView = view
    }
}



