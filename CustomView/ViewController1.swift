//
//  ViewController1.swift
//  CustomView
//
//  Created by Sven on 2020/8/12.
//  Copyright © 2020 Sven. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

    @IBOutlet weak var myViewB: MyViewB!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMyViewA()
        setupMyViewB()
    }
    
    /// 添加纯代码自定义 View
    private func setupMyViewA() {
        let view = MyViewA(frame: CGRect(x: 50, y: 100, width: 100, height: 100))
        view.label.text = "代码自定义"
        view.imageView.backgroundColor = .orange
        view.backgroundColor = .gray
        self.view.addSubview(view)
    }
    
    private func setupMyViewB() {
        myViewB.imageView.backgroundColor = .orange
        myViewB.label.text = "xib + 代码"
        myViewB.backgroundColor = .gray
    }
    
}
