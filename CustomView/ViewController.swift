//
//  ViewController.swift
//  CustomView
//
//  Created by Sven on 2020/8/11.
//  Copyright © 2020 Sven. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        printViewLifeCirl() // 查看代码初始化 生命周期
    }
    
    private func printViewLifeCirl() {
        let viewLife = ViewLifeCircle(frame: .init(x: 0, y: 50, width: UIScreen.main.bounds.width, height: 100))
        viewLife.backgroundColor = .green
        view.addSubview(viewLife)
    }


}

