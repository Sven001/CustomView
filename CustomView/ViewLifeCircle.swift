//
//  ViewLifeCircle.swift
//  CustomView
//
//  Created by Sven on 2020/8/12.
//  Copyright © 2020 Sven. All rights reserved.

//  UIView 生命周期

import UIKit

class ViewLifeCircle: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print(#function)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        print(#function)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        print(#function)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        print(#function)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        print(#function)
    }
    override func draw(_ layer: CALayer, in ctx: CGContext) {
        super.draw(layer, in: ctx)
        print(#function)
    }
    
    deinit {
        print(#function)
    }
}

